package com.hfad.horoscope;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class databaseHelper extends SQLiteOpenHelper{

    private static final String DB_NAME = "ZodiacApp";
    private static final int DB_VERSION = 1;

    private static void insertSign(SQLiteDatabase db, String name, String description, String symbol, String month){
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", name);
        contentValues.put("DESCRIPTION", description);
        contentValues.put("SYMBOL", symbol);
        contentValues.put("MONTH", month);

        db.insert("HOROSCOPE", null, contentValues);
    }

    databaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE HOROSCOPE (_id INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, DESCRIPTION TEXT, SYMBOL TEXT, MONTH TEXT)";

        db.execSQL(sql);

        insertSign(db, "Aries", "Courageous and Energetic.", "Ram", "April");
        insertSign(db, "Taurus", "Known for being reliable, practical, ambitious and sensual.", "Bull", "May");
        insertSign(db, "Gemini", "Gemini-born are clever and intellectual.", "Twins", "June");
        insertSign(db, "Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July");
        insertSign(db, "Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion", "August");
        insertSign(db, "Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September");
        insertSign(db, "Libra", "Libras are famous for maintaining balance and harmony.", "Scales", "October");
        insertSign(db, "Scorpio", "Strong willed and mysterious.", "Scorpion", "November");
        insertSign(db, "Sagittarius", "Born adventurers.", "Archer", "December");
        insertSign(db, "Capricorn", "The most determined sign in the Zodiac.", "Goat", "January");
        insertSign(db, "Aquarius", "Humanitarians to the core.", "Water Bearer", "February");
        insertSign(db, "Pisces", "Proverbial dreamers of the Zodiac.", "Fish", "March");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
