package com.hfad.horoscope;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.widget.TextView;

public class signActivity extends Activity {

    public static final String EXTRA_SIGNNO = "signNo";
    SQLiteDatabase db;
    Cursor cursor;
    private dailyHoroscope zodiacApi = new dailyHoroscope();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horoscopesign);

        int signNo = (Integer)getIntent().getExtras().get(EXTRA_SIGNNO);

        SQLiteOpenHelper databaseHelper = new databaseHelper(this);
        db = databaseHelper.getReadableDatabase();

        cursor = db.query("HOROSCOPE", new String[]{"_id", "NAME", "DESCRIPTION", "SYMBOL", "MONTH"}, "_id = ?", new String[]{Integer.toString(signNo)}, null, null, null);

        if(cursor.moveToFirst()){
            TextView name = (TextView)findViewById(R.id.name);
            String nameString = cursor.getString(1);
            name.setText(nameString);

            TextView description = (TextView)findViewById(R.id.description);
            description.setText(cursor.getString(2));

            TextView symbol = (TextView)findViewById(R.id.symbol);
            symbol.setText(cursor.getString(3));

            TextView month = (TextView)findViewById(R.id.month);
            month.setText(cursor.getString(4));

            String dailyZodiac = zodiacApi.getDailyHoroscope(nameString.toLowerCase());

            TextView daily = (TextView)findViewById(R.id.daily);
            daily.setText(dailyZodiac);
        }

        cursor.close();
        db.close();
    }
}
