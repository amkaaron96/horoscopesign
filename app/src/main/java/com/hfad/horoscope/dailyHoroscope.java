package com.hfad.horoscope;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class dailyHoroscope {

    private OkHttpClient client;

    String getDailyHoroscope(String subject) {
        client = new OkHttpClient();
        String dailyHoroscope = "";
        String response = loadContent(subject);
        if(response != null) {
            try {
                JSONObject obj = new JSONObject(response);
                dailyHoroscope = obj.getString("horoscope");

            } catch (JSONException je) {
                je.printStackTrace();
            }
        }else{
            dailyHoroscope = ("Could not get Horoscope - API is Down!");
        }
        return dailyHoroscope;
    }

    public static String GET(OkHttpClient client, HttpUrl url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private String loadContent(final String subject) {
        String response = null;
        try {
            response = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    try {
                        return GET(client, buildURL(subject));


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }.execute().get();
        }catch (InterruptedException | ExecutionException e){
            e.printStackTrace();
        }
        return response;
    }


    public HttpUrl buildURL(String sign) {
        return new HttpUrl.Builder()
                .scheme("http")
                .host("sandipbgt.com")
                .addPathSegment("theastrologer")
                .addPathSegment("api")
                .addPathSegment("horoscope")
                .addPathSegment(sign)
                .addPathSegment("today")
                .build();
    }

}