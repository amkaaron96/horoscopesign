package com.hfad.horoscope;

public class horoscopeSign {
    private String name;
    private String description;
    private String symbol;
    private String month;

    public static final horoscopeSign[] signs = {
            new horoscopeSign("Aries", "Courageous and Energetic.", "Ram", "April"),
            new horoscopeSign("Taurus", "Known for being reliable, practical, ambitious and sensual.", "Bull", "May"),
            new horoscopeSign("Gemini", "Gemini-born are clever and intellectual.", "Twins", "June"),
            new horoscopeSign("Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July"),
            new horoscopeSign("Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion", "August"),
            new horoscopeSign("Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September"),
            new horoscopeSign("Libra", "Libras are famous for maintaining balance and harmony.", "Scales", "October"),
            new horoscopeSign("Scorpio", "Strong willed and mysterious.", "Scorpion", "November"),
            new horoscopeSign("Sagittarius", "Born adventurers.", "Archer", "December"),
            new horoscopeSign("Capricorn", "The most determined sign in the Zodiac.", "Goat", "January"),
            new horoscopeSign("Aquarius", "Humanitarians to the core.", "Water Bearer", "February"),
            new horoscopeSign("Pisces", "Proverbial dreamers of the Zodiac.", "Fish", "March")
    };

    private horoscopeSign(String newName, String newDescription, String newSymbol, String newMonth) {
        this.name = newName;
        this.description = newDescription;
        this.symbol = newSymbol;
        this.month = newMonth;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getSymbol(){
        return symbol;
    }

    public String getMonth() {
        return month;
    }

    public String toString(){
        return this.name;
    }

}
